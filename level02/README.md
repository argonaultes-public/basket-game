# Test des scripts

pour tester le script python sans avoir besoin de construire l'image et créer un container basé sur cette image créée

```bash
docker run -it --rm -e PORT=9999 -p 9999:9999 -v .:/app/ -w /app/ python:3.12.3-alpine3.20 python server.py
```

pour tester le script python client sans avoir besoin de construire l'image et créer un container

```bash
docker run -it --rm -e GAME_PORT=9999 -e GAME_HOST=172.17.0.1 -v .:/app/ -w /app/ python:3.12.3-alpine3.20 python client.py
```


# Création images

## construire l'image serveur

```bash
docker build -t bga-server:latest -f Dockerfile-server .
```


## construire l'image client

```bash
docker build  -t bga-client:latest -f Dockerfile-client .
```

# Exécution

Démarrer le container server basé sur l'image

```bash
docker run --rm -e PORT=9999 -p 8888:9999 bga-server:latest
```

Démarrer le container client basé sur l'image

```bash
docker run -it --rm -e GAME_PORT=8888 -e GAME_HOST=172.17.0.1 bga-client:latest
```

Attention, sous Windows et Mac, pour accéder à la machine hôte depuis un container, utiliser `host.docker.internal`.

# Utilisation de Docker Compose

FIXME