import json
import os
import socketserver
from abc import ABC

class Config(ABC):
    pass

class ConfigInput:
    
    def __init__(self):
        pass

    def load_nickname(self):
        return str(input('Nickname: '))

    def load_nb_rows(self):
        return int(input('Nb rows: '))

    def load_nb_cols(self):
        return int(input('Nb cols: '))
    
    def load_rewards(self):
        nb_rewards = int(input('Nb rewards: '))
        rewards = []
        for _ in range(nb_rewards):
            idx_row = int(input('Index row: '))
            idx_col = int(input('Index col: '))
            rewards.append((idx_row, idx_col))
        return rewards

class ConfigFile:
    
    def __init__(self):
        self.__configfile = 'config-debug.json'
        with open(self.__configfile, 'r') as f:
            self.__configjson = json.load(f)

    def load_nickname(self):
        return self.__configjson['nickname']

    def load_nb_rows(self):
        return self.__configjson['nb_rows']

    def load_nb_cols(self):
        return self.__configjson['nb_cols']
    
    def load_rewards(self):
        rewards = []
        for reward in self.__configjson['rewards']:
            idx_row = reward['row']
            idx_col = reward['col']
            rewards.append((idx_row, idx_col))
        return rewards

class EmptyCell:
    def __repr__(self):
        return ' '

class Board:

    @classmethod
    def init_board(cls):
        print('set from config file')
        config = ConfigFile()
        board = Board(config.load_nb_rows(), config.load_nb_cols())
        for position in config.load_rewards():
            board.add_reward(Reward(), position)
        board.add_player(Player(config.load_nickname()), (0,0))
        return board

    def __init__(self, nb_rows, nb_cols):
        print('init board class')
        self.__nb_rows = nb_rows
        self.__nb_cols = nb_cols
        self.__board = [ [ EmptyCell() for _ in range(nb_cols)] for _ in range(nb_rows) ]

    def add_player(self, player, position):

        self.__board[position[0]][position[1]] = player

    def add_reward(self, reward, position):
        self.__board[position[0]][position[1]] = reward

    def find_player_position(self):
        position = ()
        for row_idx, row in enumerate(self.__board):
            for col_idx, cell in enumerate(row):
                if type(cell) == Player:
                    position = (row_idx, col_idx)
        return position

    def move_player(self, direction):
        current_position = self.find_player_position()
        match direction.upper():
            case 'N':
                new_position = (current_position[0] - 1, current_position[1])
            case 'S':
                new_position = (current_position[0] + 1, current_position[1])
            case 'W':
                new_position = (current_position[0], current_position[1] - 1)
            case 'E':
                new_position = (current_position[0], current_position[1] + 1)
            case _:
                return 1
        if new_position[0] < 0 or new_position[0] >= self.__nb_rows or new_position[1] < 0 or new_position[1] >= self.__nb_cols:
            print('Unable to move')
            return 1
        player = self.__board[current_position[0]][current_position[1]]
        self.__board[current_position[0]][current_position[1]] = EmptyCell()
        score = 0
        if type(self.__board[new_position[0]][new_position[1]]) == Reward:
            score = 2
        self.__board[new_position[0]][new_position[1]] = player
        return score


    @property
    def score(self):
        score = 0
        for row in self.__board:
            for cell in row:
                if type(cell) == Reward:
                    score += cell.score
        return score

    def display(self):
        for row in self.__board:
            for cell in row:
                print(f'| {cell} ', end='')
            print('|')


class Player:
    
    def __init__(self, nickname):
        self.__nickname = nickname

    def __repr__(self):
        return 'P'


class Reward:
    
    def __init__(self):
        self.__score = 1

    @property
    def score(self):
        return self.__score

    def __repr__(self):
        return '*'




class Game(socketserver.BaseRequestHandler):

    __board = Board.init_board()

    def __init__(self, request, client_address, server):
        print('init game class')
        super().__init__(request, client_address, server)


    def handle(self):
        print(f'{Game.__board.score}')
        if Game.__board.score <= 0:
            Game.__board = Board.init_board()
            self.request.sendall(bytes(str(3), 'ascii'))
        if Game.__board.score > 0:
            direction = self.request.recv(1024).strip()
            direction_utf8 = direction.decode("UTF-8")
            result = Game.__board.move_player(direction_utf8)
            Game.display()
            self.request.sendall(bytes(str(result), 'ascii'))
        


    @classmethod
    def display(cls):
        cls.__board.display()



if __name__ == '__main__':
        HOST, PORT = "0.0.0.0", int(os.getenv('PORT'))

        with socketserver.TCPServer((HOST, PORT), Game) as server:
            print(f'starting server listening on {PORT}')
            server.serve_forever()
            print('after serve_forever')
