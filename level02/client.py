import socket
import os

class Client:

    def get_direction(self):
        return bytes(input('direction: '), encoding='ascii')

HOST = os.getenv('GAME_HOST', 'localhost')
PORT = int(os.getenv('GAME_PORT', '9999'))

if __name__ == '__main__':
    c = Client()
    result = '0'
    while result != '1':
        direction = c.get_direction()
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            s.sendall(direction)
            data = s.recv(1024)

        result = str(data)
        print(f"Received {result}")