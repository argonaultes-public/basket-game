import json
import os

class Client:

    def get_direction(self):
        return str(input('direction: '))

class Game:
    def __init__(self):
        config = ConfigFile()
        self.__client = Client()
        self.__board = Board(config.load_nb_rows(), config.load_nb_cols())
        for position in config.load_rewards():
            self.__board.add_reward(Reward(), position)
        self.__board.add_player(Player(config.load_nickname()), (0,0))

    def start(self):
        while self.__board.score > 0:
            direction = self.__client.get_direction()
            result = self.__board.move_player(direction)
            self.display()
        print('Congrats')

    def display(self):
        os.system('clear')
        self.__board.display()

class ConfigInput:
    
    def __init__(self):
        pass

    def load_nickname(self):
        return str(input('Nickname: '))

    def load_nb_rows(self):
        return int(input('Nb rows: '))

    def load_nb_cols(self):
        return int(input('Nb cols: '))
    
    def load_rewards(self):
        nb_rewards = int(input('Nb rewards: '))
        rewards = []
        for _ in range(nb_rewards):
            idx_row = int(input('Index row: '))
            idx_col = int(input('Index col: '))
            rewards.append((idx_row, idx_col))
        return rewards

class ConfigFile:
    
    def __init__(self):
        self.__configfile = 'config.json'
        with open(self.__configfile, 'r') as f:
            self.__configjson = json.load(f)

    def load_nickname(self):
        return self.__configjson['nickname']

    def load_nb_rows(self):
        return self.__configjson['nb_rows']

    def load_nb_cols(self):
        return self.__configjson['nb_cols']
    
    def load_rewards(self):
        rewards = []
        for reward in self.__configjson['rewards']:
            idx_row = reward['row']
            idx_col = reward['col']
            rewards.append((idx_row, idx_col))
        return rewards

class EmptyCell:
    def __repr__(self):
        return ' '

class Board:

    def __init__(self, nb_rows, nb_cols):
        self.__nb_rows = nb_rows
        self.__nb_cols = nb_cols
        self.__board = [ [ EmptyCell() for _ in range(nb_cols)] for _ in range(nb_rows) ]

    def add_player(self, player, position):

        self.__board[position[0]][position[1]] = player

    def add_reward(self, reward, position):
        self.__board[position[0]][position[1]] = reward

    def find_player_position(self):
        position = ()
        for row_idx, row in enumerate(self.__board):
            for col_idx, cell in enumerate(row):
                if type(cell) == Player:
                    position = (row_idx, col_idx)
        return position

    def move_player(self, direction):
        current_position = self.find_player_position()
        match direction:
            case 'N':
                new_position = (current_position[0] - 1, current_position[1])
            case 'S':
                new_position = (current_position[0] + 1, current_position[1])
            case 'W':
                new_position = (current_position[0], current_position[1] - 1)
            case 'E':
                new_position = (current_position[0], current_position[1] + 1)
            case _:
                return 0
        if new_position[0] < 0 or new_position[0] >= self.__nb_rows or new_position[1] < 0 or new_position[1] >= self.__nb_cols:
            print('Unable to move')
            return 0
        player = self.__board[current_position[0]][current_position[1]]
        self.__board[current_position[0]][current_position[1]] = EmptyCell()
        score = 0
        if type(self.__board[new_position[0]][new_position[1]]) == Reward:
            score = self.__board[new_position[0]][new_position[1]].score
        self.__board[new_position[0]][new_position[1]] = player
        return score


    @property
    def score(self):
        score = 0
        for row in self.__board:
            for cell in row:
                if type(cell) == Reward:
                    score += cell.score
        return score

    def display(self):
        for row in self.__board:
            for cell in row:
                print(f'| {cell} ', end='')
            print('|')


class Player:
    
    def __init__(self, nickname):
        self.__nickname = nickname

    def __repr__(self):
        return 'P'


class Reward:
    
    def __init__(self):
        self.__score = 1

    @property
    def score(self):
        return self.__score

    def __repr__(self):
        return '*'

if __name__ == '__main__':
    game = Game()
    game.display()
    game.start()


