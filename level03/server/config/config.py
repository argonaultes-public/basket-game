import pathlib
from abc import ABC
import json

class Config(ABC):
    pass

class ConfigInput:
    
    def __init__(self):
        pass

    def load_nickname(self):
        return str(input('Nickname: '))

    def load_nb_rows(self):
        return int(input('Nb rows: '))

    def load_nb_cols(self):
        return int(input('Nb cols: '))
    
    def load_rewards(self):
        nb_rewards = int(input('Nb rewards: '))
        rewards = []
        for _ in range(nb_rewards):
            idx_row = int(input('Index row: '))
            idx_col = int(input('Index col: '))
            rewards.append((idx_row, idx_col))
        return rewards

class ConfigFile:
    
    def __init__(self):
        self.__configfile = pathlib.Path(__file__).parent.parent.absolute().joinpath('config-debug.json')
        with open(self.__configfile.as_posix(), 'r') as f:
            self.__configjson = json.load(f)

    def load_nickname(self):
        return self.__configjson['nickname']

    def load_nb_rows(self):
        return self.__configjson['nb_rows']

    def load_nb_cols(self):
        return self.__configjson['nb_cols']
    
    def load_rewards(self):
        rewards = []
        for reward in self.__configjson['rewards']:
            idx_row = reward['row']
            idx_col = reward['col']
            rewards.append((idx_row, idx_col))
        return rewards