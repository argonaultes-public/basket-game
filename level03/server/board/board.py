from ..config.config import ConfigFile
from ..player.player import Player


class EmptyCell:
    def __repr__(self):
        return ' '

class Board:

    @classmethod
    def init_board(cls):
        print('set from config file')
        config = ConfigFile()
        board = Board(config.load_nb_rows(), config.load_nb_cols())
        for position in config.load_rewards():
            board.add_reward(Reward(), position)
        board.add_player(Player(config.load_nickname()), (0,0))
        return board

    def __init__(self, nb_rows, nb_cols):
        print('init board class')
        self.__nb_rows = nb_rows
        self.__nb_cols = nb_cols
        self.__board = [ [ EmptyCell() for _ in range(nb_cols)] for _ in range(nb_rows) ]

    def add_player(self, player, position):

        self.__board[position[0]][position[1]] = player

    def add_reward(self, reward, position):
        self.__board[position[0]][position[1]] = reward

    def find_player_position(self):
        position = ()
        for row_idx, row in enumerate(self.__board):
            for col_idx, cell in enumerate(row):
                if type(cell) == Player:
                    position = (row_idx, col_idx)
        return position

    def move_player(self, direction):
        current_position = self.find_player_position()
        match direction.upper():
            case 'N':
                new_position = (current_position[0] - 1, current_position[1])
            case 'S':
                new_position = (current_position[0] + 1, current_position[1])
            case 'W':
                new_position = (current_position[0], current_position[1] - 1)
            case 'E':
                new_position = (current_position[0], current_position[1] + 1)
            case _:
                return 1
        if new_position[0] < 0 or new_position[0] >= self.__nb_rows or new_position[1] < 0 or new_position[1] >= self.__nb_cols:
            print('Unable to move')
            return 1
        player = self.__board[current_position[0]][current_position[1]]
        self.__board[current_position[0]][current_position[1]] = EmptyCell()
        score = 0
        if type(self.__board[new_position[0]][new_position[1]]) == Reward:
            score = 2
        self.__board[new_position[0]][new_position[1]] = player
        return score


    @property
    def score(self):
        score = 0
        for row in self.__board:
            for cell in row:
                if type(cell) == Reward:
                    score += cell.score
        return score

    def display(self):
        for row in self.__board:
            for cell in row:
                print(f'| {cell} ', end='')
            print('|')

class Reward:
    
    def __init__(self):
        self.__score = 1

    @property
    def score(self):
        return self.__score

    def __repr__(self):
        return '*'