import os
import socketserver
from .board.board import Board

class Game(socketserver.BaseRequestHandler):

    __board = Board.init_board()

    def __init__(self, request, client_address, server):
        print('init game class')
        super().__init__(request, client_address, server)


    def handle(self):
        print(f'{Game.__board.score}')
        if Game.__board.score <= 0:
            Game.__board = Board.init_board()
            self.request.sendall(bytes(str(3), 'ascii'))
        if Game.__board.score > 0:
            direction = self.request.recv(1024).strip()
            direction_utf8 = direction.decode("UTF-8")
            result = Game.__board.move_player(direction_utf8)
            Game.display()
            self.request.sendall(bytes(str(result), 'ascii'))
        


    @classmethod
    def display(cls):
        cls.__board.display()



if __name__ == '__main__':
        HOST, PORT = "0.0.0.0", int(os.getenv('PORT'))

        with socketserver.TCPServer((HOST, PORT), Game) as server:
            print(f'starting server listening on {PORT}')
            server.serve_forever()
            print('after serve_forever')
